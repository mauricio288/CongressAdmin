@extends('layouts.master')
@section('content')

	<h3>Mi perfil</h3>
	<br>

	<div class="row">
    	<div class="col s12 m4">
			<div class="input-field"> 
				<div class="form-group">
					<label class="control-label" for="">Nombre</label>
					<span class="help-block"></span>
					<input type="text" class="form-control" placeholder="{{ Auth::user()->name }}" disabled>
				</div>
			</div>
			<div class="input-field"> 
				<div class="form-group">
					<label class="control-label" for="">Apellidos</label>
					<span class="help-block"></span>
					<input type="text" class="form-control" placeholder="{{Auth::user()->last_name}}" disabled>
				</div>
			</div>
			<div class="input-field"> 
				<div class="form-group">
					<label class="control-label" for="">Curso Registrado</label>
					<span class="help-block"></span>
					<input type="text" class="form-control" placeholder="" disabled>
				</div>
			</div><div class="input-field"> 
				<div class="form-group">
					<label class="control-label" for="">Taller registrado</label>
					<span class="help-block"></span>
					<input type="text" class="form-control" placeholder="" disabled>
				</div>
			</div>
			<a class="btn waves-effect waves-light blue darken-2">Descargar diploma</a>
      	</div>
      	<div class="col s12 m4 align-center">
			<p class="card-panel flow-text">Recuerda que los nombres con los que te registraste aparecerán en tu diploma. <br>El cambio de nombre solo se podrá realizar una vez.
			<br>
			<button type="button" class="btn waves-effect waves-light blue darken-2">Solicitar cambio</button>
			</p>

      	</div>
	</div>
	


@endsection