@extends('layouts.master')
@section('content')

	<div class="row">
		<form class="col s12" method="POST" action="/register">
			{{csrf_field()}}
			<div class="row">
				<div class="input-field col s6">
					<input id="name" name="name" type="text" class="validate">
						<label for="first_name">Nombre</label>
				</div>
				<div class="input-field col s6">
					<input id="last_name"  name="last_name" type="text" class="validate">
					<label for="last_name">Apellidos</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input id="email" name="email" type="email" class="validate">
					<label for="email">Email</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input id="password" name="password" type="password" class="validate">
					<label for="password">contraseña</label>
				</div>
			</div>
			<button type="submit" class="btn waves-effect waves-light blue darken-2"><i class="material-icons right">send</i>Enviar</button>
		</form>
	</div>

@endsection