@extends('layouts.master')
@section('content')

<div class="col s12">
		<div class="input-field">
        	<input id="search" type="search" required>
        	<label class="label-icon" for="search"><i class="material-icons">search</i></label>
        	<i class="material-icons">close</i>
        </div>
	</div>
	<table class="highlight responsive-table">
		<thead>
			<tr>
				<th data-field="id" hidden>ID</th>
				<th data-field="name">Nombre</th>
				<th data-field="last_name">Apellidos</th>
				<th data-field="course">Curso</th>
				{{-- <th data-field="date">Fecha</th> --}}
				<th style="text-align: right;">Acciones</th>
			</tr>
		</thead>
	
		<tbody>
			
			@foreach($professors as $professor)
				<tr>
					<td hidden>{{$professor->id}}</td>
					<td>{{$professor->name}}</td>
					<td>{{$professor->last_name}}</td>
					<td>{{$professor->course}}</td>
					<td style="text-align: right; width: 20em">
						
						<form method="POST" action="/professors/delete/{{$professor->id}}" style="display: initial;">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons left" style="margin:0">delete</i></button>
						</form>
						
					</td>
				</tr>
			@endforeach
			
		</tbody>
	</table>
	<br>

	@include('modals.professor')

@endsection