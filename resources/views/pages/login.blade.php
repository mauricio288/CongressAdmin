@extends('layouts.master')
@section('content')

<div class="login-form z-depth-3">
    <div class="login-color">
        <div class="login-title">
            <h4>Bienvenido</h4>
        </div>
    </div>
    <div class="row">

      <form class="col s12" method="POST" action="/login">
        {{csrf_field()}}
        <div class="row">
            <div class="input-field col s12 ">
                <input id="icon_prefix" name="email" type="text" class="validate" required>
                <label for="icon_prefix">email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="icon_prefix" name="password" type="password" class="validate" required>
                <label for="icon_prefix">contraseña</label>
            </div>
        </div>
        <button type="submit" class="waves-effect waves-light btn blue darken-2">Iniciar sesión</button>
        <a href="/register" class="waves-effect waves-teal btn-flat right">Registrarse</a>

        @include('layouts.errors')

      </form>
    </div>
</div>

@endsection