@extends('layouts.master')
@section('content')

	<div class="col s12">
		<div class="input-field">
        	<input id="search" type="search" required>
        	<label class="label-icon" for="search"><i class="material-icons">search</i></label>
        	<i class="material-icons">close</i>
        </div>
	</div>
	<table class="highlight responsive-table">
		<thead>
			<tr>
				<th data-field="id" hidden>ID</th>
				<th data-field="name">Nombre</th>
				<th data-field="last_name">Apellidos</th>
				<th data-field="last_name">email</th>
				<th style="text-align: right;">Acciones</th>
			</tr>
		</thead>
	
		<tbody>
			
			@foreach($users as $user)
				<tr>
					<td hidden>{{$user->id}}</td>
					<td>{{$user->name}}</td>
					<td>{{$user->last_name}}</td>
					<td>{{$user->email}}</td>
					<td style="text-align: right; width: 20em">
						@if( $user->isAdmin )
							{{"ADMINISTRADOR"}}
						@elseif( $user->pay )
							<a class="waves-effect waves-light btn yellow darken-2"><i class="material-icons left" style="margin:0">create</i></a>
							<form method="POST" action="/users/delete/{{$user->id}}" style="display: initial;">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons left" style="margin:0">delete</i></button>
							</form>
						@elseif(!$user->isAdmin)
							<form method="POST" action="/users/update/{{$user->id}}" style="display: initial">
							 	{{ csrf_field() }}
								<button type="submit" title="Pago realiazdo" class="waves-effect waves-light btn green lighten-1"><i class="material-icons left" style="margin:0">done</i></button>
							</form>
							<form method="POST" action="/users/upgrade/{{$user->id}}" style="display: initial">
							 	{{ csrf_field() }}
								<button type="submit" title="Volver administrador" class="waves-effect waves-light btn light-blue darken-4"><i class="material-icons left" style="margin:0">vpn_key</i></button>
							</form>
							<form method="POST" action="/users/delete/{{$user->id}}" style="display: initial;">
								{{ method_field('DELETE') }}
								{{ csrf_field() }}
								<button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons left" style="margin:0">delete</i></button>
							</form>
						@endif
					</td>
				</tr>
			@endforeach
			
		</tbody>
	</table>
	<br>
	
	 


@endsection