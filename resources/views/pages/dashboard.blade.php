@extends('layouts.master')
@section('content')

	@if(!Auth::user()->isAdmin)
		<div class="row">
			<div class="col s12 m12 center-align">
				<a href="/dashboard" class="waves-effect waves-light btn blue darken-2 ">Generar ficha de pago</a>
			</div>
		</div>

		<div class="row">
			<div class="col s12 m4">
				<div class="card">
					<div class="card-image">
						<img src="http://placehold.it/300x150">
						<span class="card-title">Perfil</span>
					</div>
					<div class="card-content">
						<p>Aquí puedes ver tus datos, tus talleres registrados y descargar tu diploma </p>
					</div>
					<div class="card-action">
						<a href="/profile" class="blue-text darken-2">Ir a perfil</a>
					</div>
				</div>
			</div>
			
			<div class="col s12 m4">
				<div class="card">
					<div class="card-image">
						<img src="http://placehold.it/300x150">
						<span class="card-title">Horario</span>
					</div>
					<div class="card-content">
						<p>Revisa los horarios de los talleres y conferencias</p>
					</div>
					<div class="card-action">
						<a href="#" class="blue-text darken-2">Ir a horario</a>
					</div>
				</div>
			</div>
			

			<div class="col s12 m4">
				<div class="card">
					<div class="card-image">
						<img src="http://placehold.it/300x150">
						<span class="card-title">Conferencias y talleres</span>
					</div>
					<div class="card-content">
						<p>Aquí puedes registrar las conferencias y talleres a los que atenderas</p>
					</div>
					<div class="card-action">
						<a href="#" class="blue-text darken-2">Ir a conferencias</a>
					</div>
				</div>
			</div>
		</div>

	@else
	
		<div class="row center-align">
			<h2>Bienvenido</h2>
		</div>

		<div class="row">
			<div class="col s12 m6 center-align">
				<a href="/users" class="waves-effect waves-light btn-large blue darken-2 ">Administrar participantes</a>
			</div>
			<div class="col s12 m6 center-align">
				<a href="/courses" class="waves-effect waves-light btn-large blue darken-2 ">Administrar cursos</a>
			</div>
		</div>
		<div class="row">
			<div class="col s12 m6 center-align">
				<a href="/professors" class="waves-effect waves-light btn-large blue darken-2 ">Administrar ponentes</a>
			</div>
			{{-- <div class="col s12 m6 center-align">
				<a href="/users" class="waves-effect waves-light btn-large blue darken-2 ">Administrar cursos</a>
			</div> --}}
		</div>

	@endif


	

@endsection
