<!DOCTYPE html>
<html>
  <head>
    <title>Bienvenido </title>
    @include('layouts.header')
  </head>

  <body class="login">
    @include('layouts.nav')
    <br>
    <main>
        <div class="container login">
            
            @yield('content')
            
        </div>        
    </main>


    @include('layouts.footer')
  </body>
</html>