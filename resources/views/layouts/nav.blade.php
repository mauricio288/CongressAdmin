<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
	<li><a href="#!">Perfil</a></li>
	<li class="divider"></li>
	<li><a href="/logout">Cerrar sesión</a></li>
	
	
</ul>
<nav>
	<div class="nav-wrapper blue darken-2">

		@if( Auth::check() )
			<a href="home" class="brand-logo" ><i class="material-icons">home</i></a>
		@else
			<a href="/" class="brand-logo" >Logo</a>
		@endif
		<!-- activate side-bav in mobile view -->
		<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		<ul class="right hide-on-med-and-down">
			<!-- Dropdown Trigger -->
			@if(Auth::check())
			<li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }} {{ Auth::user()->last_name }}<i class="material-icons right">arrow_drop_down</i></a></li>
			@endif
		</ul>
		<!-- navbar for mobile -->
		<ul class="side-nav" id="mobile-demo">
			<li><a href="sass.html">Sass</a></li>
			<li><a href="components.html">Components</a></li>
		</ul>
	</div>
</nav>