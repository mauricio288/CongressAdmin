
<a class="btn-floating btn-large waves-effect waves-light blue darken-2 right pulse" href="#modal1">
	<i class="material-icons">add</i>
</a>

  <!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
  	<form class="col s12" method="POST" action="/professors">
    	<div class="modal-content">
      		<h4>Registrar nuevo ponente</h4>
			{{csrf_field()}}
			<div class="row">
				<div class="input-field col s6">
					<input id="name" name="name" type="text" class="validate" required>
						<label for="first_name">Nombre</label>
				</div>
				<div class="input-field col s6">
					<input id="last_name"  name="last_name" type="text" class="validate" required>
					<label for="last_name">Apellidos</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input id="course" name="course" type="text" class="validate" required>
					<label for="course">Nombre de curso</label>
				</div>
			</div>
			<div class="file-field input-field">
		    	<div class="btn btn yellow darken-2">
		        	<span>Subir foto</span>
		        	<input type="file">
		      	</div>
		      	<div class="file-path-wrapper">
		        	<input class="file-path validate" type="text">
		      	</div>
		    </div>
		    <p>Nota: Porás actualizar la foto usando el botón editar.</p>
		</div>

	    <div class="modal-footer">
	    	<button type="submit" class="waves-effect waves-light btn blue darken-2 ">Registrar</button>
	    	<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cerrar</a>
	    </div>
    </form>
</div>

<script>
	$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	    $('.modal').modal({
	    	dismissible: true, // Modal can be dismissed by clicking outside of the modal
	    	opacity: .5, // Opacity of modal background
	    	inDuration: 300, // Transition in duration
	        outDuration: 200, // Transition out duration
	        startingTop: '4%', // Starting top style attribute
	        endingTop: '10%', // Ending top style attribute
		});
	});
</script>
