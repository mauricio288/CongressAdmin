<!DOCTYPE html>
<html>
<head>
	<title>Error 404</title>
	<meta charset="UTF-8">
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="css/all.css"  media="screen,projection"/>
	<link rel="stylesheet" href="css/style.css">
	<!--Let browser know website is optimized for mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" ></script>

</head>
<body>

	<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
	<li><a href="#!">Perfil</a></li>
	<li class="divider"></li>
	<li><a href="/logout">Cerrar sesión</a></li>
	
	
</ul>
<nav>
	<div class="nav-wrapper blue darken-2">

		@if( Auth::check() )
			<a href="home" class="brand-logo" ><i class="material-icons">home</i></a>
		@else
			<a href="/" class="brand-logo" >Logo</a>
		@endif
		<!-- activate side-bav in mobile view -->
		<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		<ul class="right hide-on-med-and-down">
			<!-- Dropdown Trigger -->
			@if(Auth::check())
			<li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }} {{ Auth::user()->last_name }}<i class="material-icons right">arrow_drop_down</i></a></li>
			@endif
		</ul>
		<!-- navbar for mobile -->
		<ul class="side-nav" id="mobile-demo">
			<li><a href="sass.html">Sass</a></li>
			<li><a href="components.html">Components</a></li>
		</ul>
	</div>
</nav>

	<main>
	<div class="row">
		<div class="col s12 center-align ">
			<h3>Upss...la página que buscas no existe  </h3>
		</div>	
	</div>

	<div class="row">
		<div class="col s6 m12 center-align">
			<a href="/" class="waves-effect waves-light btn-large">Ir a inicio</a>
		</div>
		<br>
		<br>
		<br>
		<div class="col s6 m12 center-align">
			<a href="/" class="waves-effect waves-light btn-large">Iniciar sesión</a>
		</div>
	</div>
		
</main>



</body>
</html>
