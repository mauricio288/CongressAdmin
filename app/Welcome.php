<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Welcome extends Model
{
 	public function getRoute()
 	{
 		$routeName = Route::getCurrentRoute()->getPath();

 		return $routeName;
 	}
}
