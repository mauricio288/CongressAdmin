<?php

namespace App\Http\Controllers;
use DB;
use App\User;

class RegistrationController extends Controller
{
    public function create()
    {
    	return view('pages.register');
    }

    public function store()
    {
    	//validate form
    	$this->validate(request(), [
    		'name' => 'required',
    		'last_name' => 'required',
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	//create the user
    	$user = User::create([ 
            'name' => request('name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);

    	//sign in 
    	auth()->login($user);

    	return redirect('/');
    }

    public function updatePay($id)
    {
        DB::table('users')->where('id', $id)->update(['pay' => 1]);

        return redirect('users');
    }

    public function upgradeUser($id) 
    {
        DB::table('users')->where('id', $id)->update(['isAdmin' => 1]);

        return redirect('users');
    }

    public function deleteUser($id) 
    {
        DB::table('users')->delete($id);

        return redirect('users');
    }

}
