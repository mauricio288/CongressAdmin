<?php

namespace App\Http\Controllers;
use DB;
use App\Professor;

use Illuminate\Http\Request;

class ProfessorsRegistrationController extends Controller
{

    public function store()
    {
    	//validate form
    	$this->validate(request(), [
    		'name' => 'required',
    		'last_name' => 'required',
    		'course' => 'required',
    	]);

    	//create professor
    	$professor = Professor::create([
    		'name' => request('name'),
    		'last_name' => request('last_name'),
    		'course' => request('course'),
    		'photo_path' => request('photo')
    	]);

    	return redirect('/professors');

    }
}
