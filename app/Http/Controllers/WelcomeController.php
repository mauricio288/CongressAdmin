<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class WelcomeController extends Controller
{
    public function index()
    {
    	return view('pages.index');
    }

    public function dashboard()
    {
    	return view('pages.dashboard');
    }

    public function register()
    {
    	return view('pages.register');
    }

    public function usersAdmin()
    {
        $users = DB::table('users')->get();

        return view('pages.users', compact('users'));
    }

    public function professorsAdmin()
    {
        $professors = DB::table('professors')->get();

        return view('pages.professors', compact('professors'));
    }

    public function profile()
    {
        return view('pages.profile');
    }
}
