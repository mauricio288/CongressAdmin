<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{

	public function __construct() 
	{
		$this->middleware('guest', ['except' => 'destroy']);
	}

	public function create()
	{
		return view('pages.login');
	}

	public function store()
    {
        if(! auth()->attempt(request(['email', 'password']))) {
            return back()->withErrors([
        		'message' => 'Revisa tu usuario o contraseña'
        	]);            
        }

        return redirect('/dashboard');
    }

    public function destroy()	
    {
    	auth()->logout();

    	return redirect('/');
    }
}
