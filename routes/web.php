<?php

// GENERAL ROUTES
Route::get('/', 'WelcomeController@index');
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');
Route::get('/login', 'SessionController@create');
Route::post('/login', 'SessionController@store');

// ROUTES FOR ALL REGISTRED USER
Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'WelcomeController@dashboard')->name('home');
	Route::get('/dashboard', 'WelcomeController@dashboard');
	Route::get('/profile', 'WelcomeController@profile');
	Route::get('/logout', 'SessionController@destroy');
});

// ROUTES ONLY FOR ADMIN
Route::group(['middleware' => 'admin'], function() {
    
	Route::get('/users', 'WelcomeController@usersAdmin');
	Route::post('/users/update/{id}', 'RegistrationController@updatePay')->name('users');
	Route::post('/users/upgrade/{id}', 'RegistrationController@upgradeUser')->name('users');
	Route::delete('/users/delete/{id}', 'RegistrationController@deleteUser')->name('users');

	Route::get('/professors', 'WelcomeController@professorsAdmin');
	Route::post('/professors', 'ProfessorsRegistrationController@store');
});